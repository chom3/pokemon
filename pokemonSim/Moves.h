//
//  Moves.h
//  pokemonSim
//
//  Created by Corey Hom on 4/7/16.
//  Copyright © 2016 Corey Hom. Stanley Chan. All rights reserved.
//

@interface Move:NSObject
{
    NSString *name;
    
    NSString *_type;
    
    int _power;
    int _acc;
    int _pp;
    int _specEff;
    NSString *_effectCaused;
}
@property(nonatomic, readwrite) NSString *name;
@property(nonatomic, readwrite) int _power;
@property(nonatomic, readwrite) int _acc;
@property(nonatomic, readwrite) int _pp;
@property(nonatomic, readwrite) int _specEff;
@property(nonatomic, readwrite) NSString *_effectCaused;
@property(nonatomic, readwrite) NSString *_type;

@end

@implementation Move

@synthesize name;
@synthesize _power, _acc, _pp, _specEff;
@synthesize _effectCaused, _type;


-(id) init
{
    self = [super init];
    if (!self) return nil;
    return self;
}

- (id) initMove:(NSString*) nameIn
                power: (int) pwrIn
               acc: (int) accIn
               pp: (int) ppIn
              specEff: (int) specEffIn
   effectCaused: (NSString*) effIn
           type: (NSString*) typeIn
{
    self = [super init];
    if (self) {
        name = nameIn;
        _power = pwrIn;
        _acc = accIn;
        _pp = ppIn;
        _specEff = specEffIn;
        _effectCaused = effIn;
        _type = typeIn;
    }
    return self;
}

@end
