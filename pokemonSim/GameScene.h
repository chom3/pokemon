//
//  GameScene.h
//  pokemonSim
//

//  Copyright (c) 2016 Corey Hom. Stanley Chan. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene
{
    
    CGFloat gameScreenHeight;
    CGFloat gameScreenWidth;
}

-(void) move1Clicked;
-(void) move2Clicked;
-(void) move3Clicked;
-(void) move4Clicked;
-(void) switch1Clicked;
-(void) switch2Clicked;
-(void) switch3Clicked;

@end
