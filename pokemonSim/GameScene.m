//
//  GameScene.m
//  pokemonSim
//
//  Created by Corey Hom on 4/4/16.
//  Copyright (c) 2016 Corey Hom. Stanley Chan. All rights reserved.
//

#import "GameScene.h"
#import "Pokemon.h"

@implementation GameScene


//Global Variables
Pokemon *currentUserPokemon;
Pokemon *userPokemon1;
Pokemon *userPokemon2;
Pokemon *userPokemon3;
Pokemon *currentEnemyPokemon;
Pokemon *enemyPokemon1;
Pokemon *enemyPokemon2;
Pokemon *enemyPokemon3;

UILabel *userPokemonBox;
UILabel *enemyPokemonBox;


UILabel *userPokemonHealthBar1;
UILabel *userPokemonHealthBar2;
UILabel *userPokemonHealthValue;

UILabel *enemyPokemonHealthBar1;
UILabel *enemyPokemonHealthBar2;
UILabel *enemyPokemonHealthValue;

UIButton *moveBox;
UIButton *move1;
UIButton *move2;
UIButton *move3;
UIButton *move4;

UIButton *pokemon1;
UIButton *pokemon2;
UIButton *pokemon3;

UILabel *userPokemonName;
UILabel *userPokemonHealth;
UILabel *userPokemonStatus;

UILabel *enemyPokemonName;
UILabel *enemyPokemonHealth;
UILabel *enemyPokemonStatus;

BOOL userFirst = NO;
BOOL enemyFirst = NO;
BOOL enemyAsleep = NO;
BOOL enemyWoke = NO;
BOOL userMiss = NO;
BOOL enemyMiss = NO;
BOOL userFainted = NO;
BOOL enemyFainted = NO;
BOOL userSwitched = NO;
BOOL enemySwitched = NO;

NSString *userMove;
NSString *enemyMove;
NSString *userPokemonBeforeSwitch;
NSString *enemyPokemonBeforeSwitch;

int sleepCounter;
int switchFlag;
int cpuSwitched = 0;
-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    
    self.size = view.bounds.size;

    
    //Create Movelist
    Move *vine_whip = [[Move alloc] initMove: @"Vine Whip" power:35 acc:100 pp:20 specEff:0 effectCaused:@"None" type:@"Grass"];
    Move *body_slam = [[Move alloc] initMove: @"Body Slam" power:85 acc:100 pp:15 specEff:30 effectCaused:@"Paralyze" type:@"Normal"];
    Move *sleep_powder = [[Move alloc] initMove: @"Sleep Powder" power:0 acc:75 pp:15 specEff:100 effectCaused:@"Sleep"type:@"Normal"];
    Move *flamethrower = [[Move alloc] initMove: @"Flamethrower" power:95 acc:100 pp:20 specEff:10 effectCaused:@"Burn" type:@"Fire"];
    Move *dragon_rage = [[Move alloc] initMove: @"Dragon Rage" power:0 acc:100 pp:10 specEff:0 effectCaused:@"None" type:@"Dragon"];
    Move *earthquake = [[Move alloc] initMove: @"Earthquake" power:100 acc:100 pp:10 specEff:0 effectCaused:@"None" type:@"Ground"];
    Move *surf = [[Move alloc] initMove: @"Surf" power:95 acc:100 pp:15 specEff:0 effectCaused:@"None" type:@"Water"];
    Move *ice_beam = [[Move alloc] initMove: @"Ice Beam" power:95 acc:100 pp:10 specEff:10 effectCaused:@"Freeze" type:@"Ice"];
    Move *quick_attack = [[Move alloc] initMove: @"Quick Attack" power:40 acc:100 pp:30 specEff:0 effectCaused:@"First" type:@"Normal"];
    Move *thunderbolt = [[Move alloc] initMove: @"Thunderbolt" power:95 acc:100 pp:15 specEff:10 effectCaused:@"Paralyze" type:@"Electric"];
    Move *hyper_beam = [[Move alloc] initMove: @"Hyper Beam" power:150 acc:90 pp:5 specEff:0 effectCaused:@"Recharge" type:@"Normal"];
    Move *smog = [[Move alloc] initMove: @"Smog" power:20 acc:70 pp:20 specEff:40 effectCaused:@"Poison" type:@"Poison"];
    Move *razor_leaf = [[Move alloc] initMove: @"Razor Leaf" power:55 acc:95 pp:20 specEff:0 effectCaused:@"None" type:@"Grass"];
    
    //Create Pokemon for simulator
    Pokemon *venusaur = [[Pokemon alloc] initPokemon: @"Venusaur"
                                               curHP: 187
                                               maxHP: 187
                                                 atk: 147.0
                                                 def: 148.0
                                                spec: 167.0
                                                 spd: 145
                                               type1: @"Grass"
                                               type2: @"Poison"
                                               move1: vine_whip
                                               move2: body_slam
                                               move3: sleep_powder
                                               move4: razor_leaf
                                       currentStatus: @"Healthy"
                                               image: [SKSpriteNode spriteNodeWithImageNamed:@"venusaur.png"]];
    Pokemon *charizard = [[Pokemon alloc] initPokemon: @"Charizard"
                                                   curHP: 185
                                                maxHP: 185
                                                 atk: 149.0
                                                 def: 143.0
                                                spec: 177.0
                                                 spd: 167
                                               type1: @"Fire"
                                               type2: @"Flying"
                                               move1: flamethrower
                                               move2: body_slam
                                               move3: dragon_rage
                                               move4: earthquake
                                       currentStatus: @"Healthy"
                                       image: [SKSpriteNode spriteNodeWithImageNamed:@"charizard.png"]];
    Pokemon *blastoise = [[Pokemon alloc] initPokemon: @"Blastoise"
                                                   curHP: 186
                                                maxHP: 186
                                                 atk: 148.0
                                                 def: 167.0
                                                spec: 150.0
                                                 spd: 143
                                               type1: @"Water"
                                               type2: @"None"
                                               move1: surf
                                               move2: body_slam
                                               move3: ice_beam
                                               move4: earthquake
                                        currentStatus: @"Healthy"
                                                image: [SKSpriteNode spriteNodeWithImageNamed:@"blastoise.png"]];
    Pokemon *vaporeon = [[Pokemon alloc] initPokemon: @"Vaporeon"
                                                  curHP: 237
                                               maxHP: 237
                                                 atk: 128.0
                                                 def: 123.0
                                                spec: 178.0
                                                 spd: 128
                                               type1: @"Water"
                                               type2: @"None"
                                               move1: surf
                                               move2: body_slam
                                               move3: ice_beam
                                               move4: quick_attack
                                       currentStatus: @"Healthy"
                                               image: [SKSpriteNode spriteNodeWithImageNamed:@"vaporeon.png"]];
    Pokemon *flareon = [[Pokemon alloc] initPokemon: @"Flareon"
                                                  curHP: 172
                                              maxHP: 172
                                                 atk: 200.0
                                                 def: 123.0
                                                spec: 178.0
                                                 spd: 128
                                               type1: @"Fire"
                                               type2: @"None"
                                               move1: flamethrower
                                               move2: body_slam
                                               move3: smog
                                               move4: quick_attack
                                      currentStatus: @"Healthy"
                                              image: [SKSpriteNode spriteNodeWithImageNamed:@"flareon.png"]];
    Pokemon *jolteon = [[Pokemon alloc] initPokemon: @"Jolteon"
                                                 curHP: 172
                                              maxHP: 172
                                                 atk: 128.0
                                                 def: 123.0
                                                spec: 178.0
                                                 spd: 200
                                               type1: @"Electric"
                                               type2: @"None"
                                               move1: thunderbolt
                                               move2: body_slam
                                               move3: hyper_beam
                                               move4: quick_attack
                                      currentStatus: @"Healthy"
                                              image: [SKSpriteNode spriteNodeWithImageNamed:@"jolteon.png"]];
    
    
    //Start the battle!
    currentUserPokemon = venusaur;
    
    userPokemon1 = venusaur;
    userPokemon2 = blastoise;
    userPokemon3 = charizard;
    
    currentEnemyPokemon = flareon;
    
    enemyPokemon1 = flareon;
    enemyPokemon2 = vaporeon;
    enemyPokemon3 = jolteon;
    
    //Set up the window!
    gameScreenHeight = CGRectGetHeight(self.scene.view.bounds);
    gameScreenWidth = CGRectGetWidth(self.scene.view.bounds);
    
    moveBox = [[UIButton alloc]initWithFrame:CGRectMake(0, gameScreenHeight*(3.0/4.0), gameScreenWidth*(2.0/3.0), gameScreenHeight*(1.0/4.0))];
    moveBox.layer.borderColor = [UIColor cyanColor].CGColor;
    moveBox.layer.borderWidth = 10.0f;
    [moveBox setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    moveBox.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    moveBox.titleLabel.textAlignment = NSTextAlignmentCenter;
    [moveBox setHidden:YES];
    [moveBox addTarget:self action:@selector(waitUntilDialogueClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scene.view addSubview:moveBox];
    
    move1 = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(moveBox.frame), CGRectGetMinY(moveBox.frame), CGRectGetWidth(moveBox.frame), CGRectGetHeight(moveBox.frame)*(1.0/4.0))];
    move1.layer.borderColor = [UIColor purpleColor].CGColor;
    move1.layer.borderWidth = 1.0f;
    [move1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [move1 setTitle: currentUserPokemon.move1.name forState: UIControlStateNormal];
    [move1 addTarget:self action:@selector(move1Clicked) forControlEvents:UIControlEventTouchUpInside];
    [self.scene.view addSubview:move1];
    
    move2 = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(moveBox.frame), CGRectGetMinY(moveBox.frame) + CGRectGetHeight(moveBox.frame)*(1.0/4.0) , CGRectGetWidth(moveBox.frame), CGRectGetHeight(moveBox.frame)*(1.0/4.0))];
    move2.layer.borderColor = [UIColor purpleColor].CGColor;
    move2.layer.borderWidth = 1.0f;
    [move2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [move2 setTitle: currentUserPokemon.move2.name forState: UIControlStateNormal];
    [move2 addTarget:self action:@selector(move2Clicked) forControlEvents:UIControlEventTouchUpInside];
    [self.scene.view addSubview:move2];
    
    move3 = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(moveBox.frame), CGRectGetMinY(moveBox.frame) + CGRectGetHeight(moveBox.frame)*(2.0/4.0), CGRectGetWidth(moveBox.frame), CGRectGetHeight(moveBox.frame)*(1.0/4.0))];
    move3.layer.borderColor = [UIColor purpleColor].CGColor;
    move3.layer.borderWidth = 1.0f;
    [move3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [move3 setTitle: currentUserPokemon.move3.name forState: UIControlStateNormal];
    [move3 addTarget:self action:@selector(move3Clicked) forControlEvents:UIControlEventTouchUpInside];
    [self.scene.view addSubview:move3];
    
    move4 = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(moveBox.frame), CGRectGetMinY(moveBox.frame) + CGRectGetHeight(moveBox.frame)*(3.0/4.0), CGRectGetWidth(moveBox.frame), CGRectGetHeight(moveBox.frame)*(1.0/4.0))];
    move4.layer.borderColor = [UIColor purpleColor].CGColor;
    move4.layer.borderWidth = 1.0f;
    [move4 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [move4 setTitle: currentUserPokemon.move4.name forState: UIControlStateNormal];
    [move4 addTarget:self action:@selector(move4Clicked) forControlEvents:UIControlEventTouchUpInside];
    [self.scene.view addSubview:move4];
    
    //switching pokemon
    
    UILabel *switchPokemonBox = [[UILabel alloc]initWithFrame:CGRectMake(gameScreenWidth*(2.0/3.0), gameScreenHeight*(3.0/4.0), gameScreenWidth*(1.0/3.0), gameScreenHeight*(1.0/4.0))];
    switchPokemonBox.layer.borderColor = [UIColor redColor].CGColor;
    switchPokemonBox.layer.borderWidth = 1.0f;
    [self.scene.view addSubview:switchPokemonBox];
    
    
    pokemon1 = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(switchPokemonBox.frame), CGRectGetMinY(switchPokemonBox.frame), CGRectGetWidth(switchPokemonBox.frame), CGRectGetHeight(switchPokemonBox.frame)*(1.0/3.0))];
    pokemon1.layer.borderColor = [UIColor orangeColor].CGColor;
    pokemon1.layer.borderWidth = 1.0f;
    [pokemon1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [pokemon1 setTitle: currentUserPokemon.name forState: UIControlStateNormal];
    [pokemon1 addTarget:self action:@selector(switch1Clicked) forControlEvents:UIControlEventTouchUpInside];
    [self.scene.view addSubview:pokemon1];
    
    pokemon2 = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(switchPokemonBox.frame), CGRectGetMinY(switchPokemonBox.frame) + CGRectGetHeight(switchPokemonBox.frame)*(1.0/3.0), CGRectGetWidth(switchPokemonBox.frame), CGRectGetHeight(switchPokemonBox.frame)*(1.0/3.0))];
    pokemon2.layer.borderColor = [UIColor orangeColor].CGColor;
    pokemon2.layer.borderWidth = 1.0f;
    [pokemon2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [pokemon2 setTitle: userPokemon2.name forState: UIControlStateNormal];
    [pokemon2 addTarget:self action:@selector(switch2Clicked) forControlEvents:UIControlEventTouchUpInside];
    [self.scene.view addSubview:pokemon2];
    
    pokemon3 = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(switchPokemonBox.frame), CGRectGetMinY(switchPokemonBox.frame) +  CGRectGetHeight(switchPokemonBox.frame)*(2.0/3.0), CGRectGetWidth(switchPokemonBox.frame), CGRectGetHeight(switchPokemonBox.frame)*(1.0/3.0))];
    pokemon3.layer.borderColor = [UIColor orangeColor].CGColor;
    pokemon3.layer.borderWidth = 1.0f;
    [pokemon3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [pokemon3 setTitle: userPokemon3.name forState: UIControlStateNormal];
    [pokemon3 addTarget:self action:@selector(switch3Clicked) forControlEvents:UIControlEventTouchUpInside];
    [self.scene.view addSubview:pokemon3];
    
    //    [moveLeftButton setImage:moveLeftImage forState:UIControlStateNormal];
    
    
    
    //user pokemon stuff
    
    userPokemonBox = [[UILabel alloc]initWithFrame:CGRectMake(0,gameScreenHeight*(3.0/8.0),gameScreenWidth*(1.0/2.0),gameScreenHeight*(3.0/8.0))];
    //userPokemonBox.layer.borderColor =[UIColor blackColor].CGColor;
    userPokemonBox.layer.borderWidth = 1.0f;
    [self.scene.view addSubview:userPokemonBox];
    
    userPokemonName = [[UILabel alloc] initWithFrame:CGRectMake(gameScreenWidth*(1.0/2.0), CGRectGetMinY(userPokemonBox.frame) + CGRectGetHeight(userPokemonBox.frame)* (3.0/10.0), gameScreenWidth*(1.0/2.0), CGRectGetHeight(userPokemonBox.frame)* (1.0/5.0)/ 2.0)];
    userPokemonName.layer.borderColor = [UIColor greenColor].CGColor;
    userPokemonName.layer.borderWidth = 1.0F;
    userPokemonName.text = [NSString stringWithFormat: @"%@", currentUserPokemon.name];
    [userPokemonName setTextAlignment: NSTextAlignmentCenter];
    
    [self.scene.view addSubview:userPokemonName];
    
    userPokemonHealth = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(userPokemonBox.frame), CGRectGetMinY(userPokemonBox.frame) + CGRectGetHeight(userPokemonBox.frame)* (2.0/5.0), gameScreenWidth*(1.0/2.0), CGRectGetHeight(userPokemonBox.frame)* (1.0/5.0))];
    userPokemonHealth.layer.borderColor = [UIColor greenColor].CGColor;
    userPokemonHealth.layer.borderWidth = 1.0F;
    [self.scene.view addSubview:userPokemonHealth];
    
    
    userPokemonHealthBar1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(userPokemonHealth.frame), CGRectGetMinY(userPokemonHealth.frame), CGRectGetWidth(userPokemonHealth.frame), CGRectGetHeight(userPokemonHealth.frame)* (1.0/2.0))];
    userPokemonHealthBar1.layer.borderColor = [UIColor blackColor].CGColor;
    userPokemonHealthBar1.layer.borderWidth = 1.0F;
    userPokemonHealthBar1.backgroundColor = [UIColor greenColor];
    [self.scene.view addSubview:userPokemonHealthBar1];
    
    userPokemonHealthBar2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(userPokemonHealthBar1.frame), CGRectGetMinY(userPokemonHealth.frame), CGRectGetWidth(userPokemonHealth.frame) - CGRectGetWidth(userPokemonHealthBar1.frame), CGRectGetHeight(userPokemonHealth.frame)* (1.0/2.0))];
    userPokemonHealthBar2.layer.borderColor = [UIColor blackColor].CGColor;
    userPokemonHealthBar2.layer.borderWidth = 1.0F;
    userPokemonHealthBar2.backgroundColor = [UIColor yellowColor];
    [self.scene.view addSubview:userPokemonHealthBar2];
    
    userPokemonStatus = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(userPokemonHealth.frame),CGRectGetMinY(userPokemonHealth.frame) +  CGRectGetHeight(userPokemonHealth.frame)* (1.0/2.0), CGRectGetWidth(userPokemonHealth.frame)/ 2.0, CGRectGetHeight(userPokemonHealth.frame)* (1.0/2.0))];
    userPokemonStatus.layer.borderColor = [UIColor greenColor].CGColor;
    userPokemonStatus.layer.borderWidth = 1.0F;
    userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
    [userPokemonStatus setTextAlignment: NSTextAlignmentCenter];
    [self.scene.view addSubview:userPokemonStatus];
    
    userPokemonHealthValue = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMidX(userPokemonHealth.frame), CGRectGetMinY(userPokemonHealth.frame) +  CGRectGetHeight(userPokemonHealth.frame)* (1.0/2.0), CGRectGetWidth(userPokemonHealth.frame)/ 2.0, CGRectGetHeight(userPokemonHealth.frame)* (1.0/2.0))];
    userPokemonHealthValue.layer.borderColor = [UIColor redColor].CGColor;
    userPokemonHealthValue.layer.borderWidth = 1.0F;
    userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
    [userPokemonHealthValue setTextAlignment: NSTextAlignmentCenter];
    [self.scene.view addSubview:userPokemonHealthValue];
    
    //enemy pokemon stuff
    
    enemyPokemonBox = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(userPokemonBox.frame), 0, gameScreenWidth*(1.0/2.0), gameScreenHeight*(3.0/8.0))];
    //enemyPokemonBox.layer.borderColor = [UIColor blackColor].CGColor;
    enemyPokemonBox.layer.borderWidth = 1.0f;
    [self.scene.view addSubview:enemyPokemonBox];
    
    enemyPokemonName = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMinY(enemyPokemonBox.frame) + CGRectGetHeight(userPokemonBox.frame)* (3.0/10.0), gameScreenWidth*(1.0/2.0), CGRectGetHeight(userPokemonBox.frame)* (1.0/5.0) / 2.0)];
    enemyPokemonName.layer.borderColor = [UIColor greenColor].CGColor;
    enemyPokemonName.layer.borderWidth = 1.0F;
    enemyPokemonName.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.name];
    [enemyPokemonName setTextAlignment: NSTextAlignmentCenter];
    [self.scene.view addSubview:enemyPokemonName];
    
    enemyPokemonHealth = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMinY(enemyPokemonBox.frame) + CGRectGetHeight(userPokemonBox.frame)* (2.0/5.0), gameScreenWidth*(1.0/2.0), CGRectGetHeight(userPokemonBox.frame)* (1.0/5.0))];
    enemyPokemonHealth.layer.borderColor = [UIColor greenColor].CGColor;
    enemyPokemonHealth.layer.borderWidth = 1.0F;
    [self.scene.view addSubview:enemyPokemonHealth];
    
    enemyPokemonHealthBar1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(enemyPokemonHealth.frame), CGRectGetMinY(enemyPokemonHealth.frame), CGRectGetWidth(enemyPokemonHealth.frame), CGRectGetHeight(enemyPokemonHealth.frame)* (1.0/2.0))];
    enemyPokemonHealthBar1.layer.borderColor = [UIColor blackColor].CGColor;
    enemyPokemonHealthBar1.layer.borderWidth = 1.0F;
    enemyPokemonHealthBar1.backgroundColor = [UIColor greenColor];
    [self.scene.view addSubview:enemyPokemonHealthBar1];
    
    enemyPokemonHealthBar2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(enemyPokemonHealthBar1.frame), CGRectGetMinY(enemyPokemonHealth.frame), CGRectGetWidth(enemyPokemonHealth.frame) - CGRectGetWidth(enemyPokemonHealthBar1.frame), CGRectGetHeight(enemyPokemonHealth.frame)* (1.0/2.0))];
    enemyPokemonHealthBar2.layer.borderColor = [UIColor blackColor].CGColor;
    enemyPokemonHealthBar2.layer.borderWidth = 1.0F;
    enemyPokemonHealthBar2.backgroundColor = [UIColor yellowColor];
    [self.scene.view addSubview:enemyPokemonHealthBar2];
    
    enemyPokemonStatus = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(enemyPokemonHealth.frame), CGRectGetMinY(enemyPokemonHealth.frame) +  CGRectGetHeight(enemyPokemonHealth.frame)* (1.0/2.0), CGRectGetWidth(enemyPokemonHealth.frame) / 2.0, CGRectGetHeight(enemyPokemonHealth.frame)* (1.0/2.0))];
    enemyPokemonStatus.layer.borderColor = [UIColor greenColor].CGColor;
    enemyPokemonStatus.layer.borderWidth = 1.0F;
    enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
    [enemyPokemonStatus setTextAlignment: NSTextAlignmentCenter];
    [self.scene.view addSubview:enemyPokemonStatus];
    
    
    enemyPokemonHealthValue = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMidX(enemyPokemonHealth.frame), CGRectGetMinY(enemyPokemonHealth.frame) +  CGRectGetHeight(enemyPokemonHealth.frame)* (1.0/2.0), CGRectGetWidth(enemyPokemonHealth.frame) / 2.0, CGRectGetHeight(enemyPokemonHealth.frame)* (1.0/2.0))];
    enemyPokemonHealthValue.layer.borderColor = [UIColor redColor].CGColor;
    enemyPokemonHealthValue.layer.borderWidth = 1.0F;
    enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
    [enemyPokemonHealthValue setTextAlignment: NSTextAlignmentCenter];
    [self.scene.view addSubview:enemyPokemonHealthValue];
    
    
    [self scaleImage:currentUserPokemon];
    [self scaleImage:currentEnemyPokemon];
    
    [self addChild: currentUserPokemon.image];
    [self addChild: currentEnemyPokemon.image];
    
    //Set up image of the currentUserPokemon and currentEnemyPokemon
    [self setInitialUserPosition:currentUserPokemon];
    [self setInitialEnemyPosition:currentEnemyPokemon];
    
  //  [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : 50];
   // [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : 25];
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    [super touchesBegan:touches  withEvent:event];
    [self.nextResponder touchesBegan:touches withEvent:event];
    
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    if ([currentEnemyPokemon.currentStatus isEqualToString: @"FAINTED"])
    {
        //If Venusaur is out
        if ([currentUserPokemon.type1 isEqualToString:@"Grass"])
        {
            //Order of enemy pokemon choice depending if pokemon fainted
            //flareon -> jolteon -> vaporeon
            if ( !([enemyPokemon1.currentStatus isEqualToString: @"FAINTED"]))
            {
                currentEnemyPokemon = enemyPokemon1;
                enemySwitched = YES;
                [self enemySwitch:enemyPokemon1];
            }
            else if (!([enemyPokemon3.currentStatus isEqualToString: @"FAINTED"])){
                currentEnemyPokemon = enemyPokemon3;
                enemySwitched = YES;
                [self enemySwitch:enemyPokemon3];
                
            }
            else if (!([enemyPokemon2.currentStatus isEqualToString: @"FAINTED"])){
                currentEnemyPokemon = enemyPokemon2;
                enemySwitched = YES;
                [self enemySwitch:enemyPokemon2];
            }
            else{
                [self hideMoves];
                [moveBox setHidden: NO];
                [moveBox setTitle: @"You win. Play again!" forState:UIControlStateNormal];
                [moveBox addTarget:self action:@selector(waitUntilDialogueClicked) forControlEvents:UIControlEventTouchUpInside];
                [self resetGame];
            }
        }
        //If Charizard is out
        else if([currentUserPokemon.type1 isEqualToString:@"Fire"]){
            //Order of enemy pokemon choice depending if pokemon fainted
            //vaporeon -> jolteon -> flareon
            if ( !([enemyPokemon2.currentStatus isEqualToString: @"FAINTED"]))
            {
                currentEnemyPokemon = enemyPokemon2;
                enemySwitched = YES;
                [self enemySwitch:enemyPokemon2];
            }
            else if (!([enemyPokemon3.currentStatus isEqualToString: @"FAINTED"])){
                currentEnemyPokemon = enemyPokemon3;
                enemySwitched = YES;
                [self enemySwitch:enemyPokemon3];
                
            }
            else if (!([enemyPokemon1.currentStatus isEqualToString: @"FAINTED"])){
                currentEnemyPokemon = enemyPokemon1;
                enemySwitched = YES;
                [self enemySwitch:enemyPokemon1];
            }
            else{
                [self hideMoves];
                [moveBox setHidden: NO];
                [moveBox setTitle: @"You win. Play again!" forState:UIControlStateNormal];
                [moveBox addTarget:self action:@selector(waitUntilDialogueClicked) forControlEvents:UIControlEventTouchUpInside];
                [self resetGame];
            }
        }
        //If Blastoise is out
        else if([currentUserPokemon.type1 isEqualToString:@"Water"]){
            //Order of enemy pokemon choice depending if pokemon fainted
            //jolteon -> vaporeon -> flareon
            if ( !([enemyPokemon3.currentStatus isEqualToString: @"FAINTED"]))
            {
                currentEnemyPokemon = enemyPokemon3;
                enemySwitched = YES;
                [self enemySwitch:enemyPokemon3];
            }
            else if (!([enemyPokemon2.currentStatus isEqualToString: @"FAINTED"])){
                currentEnemyPokemon = enemyPokemon2;
                enemySwitched = YES;
                [self enemySwitch:enemyPokemon2];
                
            }
            else if (!([enemyPokemon1.currentStatus isEqualToString: @"FAINTED"])){
                currentEnemyPokemon = enemyPokemon1;
                enemySwitched = YES;
                [self enemySwitch:enemyPokemon1];
            }
            else{
                [self hideMoves];
                [moveBox setHidden: NO];
                [moveBox setTitle: @"You win. Play again!" forState:UIControlStateNormal];
                [moveBox addTarget:self action:@selector(waitUntilDialogueClicked) forControlEvents:UIControlEventTouchUpInside];
                [self resetGame];
            }
        }
    }
    
    if ([userPokemon1.currentStatus isEqualToString:@"FAINTED"] && [userPokemon2.currentStatus isEqualToString:@"FAINTED"] && [userPokemon3.currentStatus isEqualToString:@"FAINTED"])
    {
        [self hideMoves];
        [moveBox setHidden: NO];
        [moveBox setTitle: @"You lose. The game is over!" forState:UIControlStateNormal];
        [moveBox addTarget:self action:@selector(waitUntilDialogueClicked) forControlEvents:UIControlEventTouchUpInside];
        [self resetGame];
    }
    if([userPokemon1.currentStatus isEqualToString:@"FAINTED"]){
        [pokemon1 setEnabled:NO];
        pokemon1.backgroundColor = [UIColor redColor];
    }
    
    if([userPokemon2.currentStatus isEqualToString:@"FAINTED"]){
        [pokemon2 setEnabled:NO];
        pokemon2.backgroundColor = [UIColor redColor];
    }
    
    if([userPokemon3.currentStatus isEqualToString:@"FAINTED"]){
        [pokemon3 setEnabled:NO];
        pokemon3.backgroundColor = [UIColor redColor];
    }
}


-(void)cpuResponseToWeakness
{
    //Switch to a favorable matchup
    if([currentEnemyPokemon.type1 isEqualToString: @"Fire"] && [currentUserPokemon.type1 isEqualToString: @"Water"]){
        if (![enemyPokemon3.currentStatus isEqualToString: @"FAINTED"])
        {
            enemySwitched = YES;
            cpuSwitched = 1;
            [self removeAllChildren];
            currentEnemyPokemon = enemyPokemon3;
            
            [self scaleImage:currentUserPokemon];
            [self scaleImage:currentEnemyPokemon];
            [self setInitialUserPosition:currentUserPokemon];
            [self setInitialEnemyPosition:currentEnemyPokemon];
            
            userPokemonName.text = [NSString stringWithFormat: @"%@", currentUserPokemon.name];
            userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
            userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            
            enemyPokemonName.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.name];
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
            enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            
            [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
            
            
            [move1 setTitle: currentUserPokemon.move1.name forState: UIControlStateNormal];
            
            [move2 setTitle: currentUserPokemon.move2.name forState: UIControlStateNormal];
            
            [move3 setTitle: currentUserPokemon.move3.name forState: UIControlStateNormal];
            
            [move4 setTitle: currentUserPokemon.move4.name forState: UIControlStateNormal];
            [self addChild: currentUserPokemon.image];
            [self addChild: currentEnemyPokemon.image];
        }
    }
    else if ( [currentEnemyPokemon.type1 isEqualToString: @"Fire"] && [currentUserPokemon.type1 isEqualToString:@"Fire"])
    {
        if (![enemyPokemon2.currentStatus isEqualToString: @"FAINTED"])
        {
            enemySwitched = YES;
            cpuSwitched = 1;
            [self removeAllChildren];
            currentEnemyPokemon = enemyPokemon2;
        
            [self scaleImage:currentUserPokemon];
            [self scaleImage:currentEnemyPokemon];
            [self setInitialUserPosition:currentUserPokemon];
            [self setInitialEnemyPosition:currentEnemyPokemon];
        
            userPokemonName.text = [NSString stringWithFormat: @"%@", currentUserPokemon.name];
            userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
            userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            
            enemyPokemonName.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.name];
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
            enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
        
            [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
        
        
            [move1 setTitle: currentUserPokemon.move1.name forState: UIControlStateNormal];
        
            [move2 setTitle: currentUserPokemon.move2.name forState: UIControlStateNormal];
        
            [move3 setTitle: currentUserPokemon.move3.name forState: UIControlStateNormal];
        
            [move4 setTitle: currentUserPokemon.move4.name forState: UIControlStateNormal];
            [self addChild: currentUserPokemon.image];
            [self addChild: currentEnemyPokemon.image];
    }
        else if (![enemyPokemon3.currentStatus isEqualToString:@"FAINTED"])
        {
            enemySwitched = YES;
            cpuSwitched = 1;
            [self removeAllChildren];
            currentEnemyPokemon = enemyPokemon3;
            
            [self scaleImage:currentUserPokemon];
            [self scaleImage:currentEnemyPokemon];
            [self setInitialUserPosition:currentUserPokemon];
            [self setInitialEnemyPosition:currentEnemyPokemon];
            
            userPokemonName.text = [NSString stringWithFormat: @"%@", currentUserPokemon.name];
            userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
            userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            
            enemyPokemonName.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.name];
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
            enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            
            [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
            
            
            [move1 setTitle: currentUserPokemon.move1.name forState: UIControlStateNormal];
            
            [move2 setTitle: currentUserPokemon.move2.name forState: UIControlStateNormal];
            
            [move3 setTitle: currentUserPokemon.move3.name forState: UIControlStateNormal];
            
            [move4 setTitle: currentUserPokemon.move4.name forState: UIControlStateNormal];
            [self addChild: currentUserPokemon.image];
            [self addChild: currentEnemyPokemon.image];
        }
    }
    else if ([currentEnemyPokemon.type1 isEqualToString: @"Water"] && [currentUserPokemon.type1 isEqualToString: @"Grass"])
    {
        if (![enemyPokemon1.currentStatus isEqualToString: @"FAINTED"])
        {
            enemySwitched = YES;
            cpuSwitched = 1;
            [self removeAllChildren];
            currentEnemyPokemon = enemyPokemon1;
            
            [self scaleImage:currentUserPokemon];
            [self scaleImage:currentEnemyPokemon];
            [self setInitialUserPosition:currentUserPokemon];
            [self setInitialEnemyPosition:currentEnemyPokemon];
            
            userPokemonName.text = [NSString stringWithFormat: @"%@", currentUserPokemon.name];
            userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
            userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            
            enemyPokemonName.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.name];
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
            enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            
            [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
            
            
            [move1 setTitle: currentUserPokemon.move1.name forState: UIControlStateNormal];
            
            [move2 setTitle: currentUserPokemon.move2.name forState: UIControlStateNormal];
            
            [move3 setTitle: currentUserPokemon.move3.name forState: UIControlStateNormal];
            
            [move4 setTitle: currentUserPokemon.move4.name forState: UIControlStateNormal];
            [self addChild: currentUserPokemon.image];
            [self addChild: currentEnemyPokemon.image];
        }
        else if (![enemyPokemon3.currentStatus isEqualToString:@"FAINTED"])
        {
            enemySwitched = YES;
            cpuSwitched = 1;
            [self removeAllChildren];
            currentEnemyPokemon = enemyPokemon3;
            
            [self scaleImage:currentUserPokemon];
            [self scaleImage:currentEnemyPokemon];
            [self setInitialUserPosition:currentUserPokemon];
            [self setInitialEnemyPosition:currentEnemyPokemon];
            
            userPokemonName.text = [NSString stringWithFormat: @"%@", currentUserPokemon.name];
            userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
            userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            
            enemyPokemonName.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.name];
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
            enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            
            [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
            
            
            [move1 setTitle: currentUserPokemon.move1.name forState: UIControlStateNormal];
            
            [move2 setTitle: currentUserPokemon.move2.name forState: UIControlStateNormal];
            
            [move3 setTitle: currentUserPokemon.move3.name forState: UIControlStateNormal];
            
            [move4 setTitle: currentUserPokemon.move4.name forState: UIControlStateNormal];
            [self addChild: currentUserPokemon.image];
            [self addChild: currentEnemyPokemon.image];
        }
    }
}

-(NSString *)cpuResponse
{
    //use super effective attack
    //if none, use highest base power attack
    //if currentUserHealth < some low number, use quick attack
    CGFloat damage;
    if ([currentEnemyPokemon.name isEqualToString: @"Vaporeon"] && [currentUserPokemon.name isEqualToString: @"Charizard"])
    {
        damage = [self damageCalculator:currentEnemyPokemon Pokemon2:currentUserPokemon move:currentEnemyPokemon.move1];
    
        if (switchFlag == 1)
        {
            int predictSwitch = arc4random() % 100;
            if (predictSwitch < 65)
            {
                damage = [self damageCalculator:currentEnemyPokemon Pokemon2:currentUserPokemon move:currentEnemyPokemon.move2];
            }
        }
        NSLog(@"Damage calculated: %f", damage);
        
        CGFloat willHit = arc4random() % 100;
        enemyMove = currentEnemyPokemon.move1.name;
        if (willHit <= currentEnemyPokemon.move1._acc)
        {            currentUserPokemon._curHP = currentUserPokemon._curHP - damage;
            userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
            userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            if (currentUserPokemon._curHP <= 0)
            {
                //enemy has fainted!
                userFainted = YES;
                currentUserPokemon.currentStatus = @"FAINTED";
                
                currentUserPokemon._curHP = 0;
                userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
                userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];

            }
            [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
            return currentEnemyPokemon.move1.name;
        }
        else{
            enemyMiss = YES;
        }
    }
    else if ([currentEnemyPokemon.name isEqualToString: @"Vaporeon"] && [currentUserPokemon.name isEqualToString: @"Blastoise"])
    {
        damage = [self damageCalculator:currentEnemyPokemon Pokemon2:currentUserPokemon move:currentEnemyPokemon.move2];
        NSLog(@"Damage calculated: %f", damage);
        
        CGFloat willHit = arc4random() % 100;
        enemyMove = currentEnemyPokemon.move2.name;
        if (willHit <= currentEnemyPokemon.move2._acc)
        {
            currentUserPokemon._curHP = currentUserPokemon._curHP - damage;
            userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
            userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            if (currentUserPokemon._curHP <= 0)
            {
                //enemy has fainted!
                userFainted = YES;
                currentUserPokemon.currentStatus = @"FAINTED";
                currentUserPokemon._curHP = 0;
                userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
                userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];

            }
            [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
            return currentEnemyPokemon.move2.name;
        }
        else{
            enemyMiss = YES;
        }
    }
    else if ([currentEnemyPokemon.name isEqualToString: @"Vaporeon"] && [currentUserPokemon.name isEqualToString: @"Venusaur"])
    {
        damage = [self damageCalculator:currentEnemyPokemon Pokemon2:currentUserPokemon move:currentEnemyPokemon.move3];
        NSLog(@"Damage calculated: %f", damage);
        
        CGFloat willHit = arc4random() % 100;
        enemyMove = currentEnemyPokemon.move3.name;
        if (willHit <= currentEnemyPokemon.move3._acc)
        {
            currentUserPokemon._curHP = currentUserPokemon._curHP - damage;
            userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
            userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            if (currentUserPokemon._curHP <= 0)
            {
                //enemy has fainted!
                userFainted = YES;
                currentUserPokemon.currentStatus = @"FAINTED";
                currentUserPokemon._curHP = 0;
                userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
                userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];

            }
            [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
            return currentEnemyPokemon.move3.name;
        }
        else{
            enemyMiss = YES;
        }
    }
    else if ([currentEnemyPokemon.name isEqualToString: @"Flareon"] && [currentUserPokemon.name isEqualToString: @"Venusaur"])
    {
        damage = [self damageCalculator:currentEnemyPokemon Pokemon2:currentUserPokemon move:currentEnemyPokemon.move1];
        NSLog(@"Damage calculated: %f", damage);
        if (switchFlag == 1)
        {
            int predictSwitch = arc4random() % 100;
            if (predictSwitch < 65)
            {
                damage = [self damageCalculator:currentEnemyPokemon Pokemon2:currentUserPokemon move:currentEnemyPokemon.move2];
            }
        }
        enemyMove = currentEnemyPokemon.move1.name;
        CGFloat willHit = arc4random() % 100;
        if (willHit <= currentEnemyPokemon.move1._acc)
        {
            currentUserPokemon._curHP = currentUserPokemon._curHP - damage;
            userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
            userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            if (currentUserPokemon._curHP <= 0)
            {
                //enemy has fainted!
                userFainted = YES;
                currentUserPokemon.currentStatus = @"FAINTED";
                currentUserPokemon._curHP = 0;
                userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
                userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];


            }
            [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
            return currentEnemyPokemon.move1.name;
        }
        else{
            enemyMiss = YES;
        }
    }
    else if ([currentEnemyPokemon.name isEqualToString: @"Flareon"] && [currentUserPokemon.name isEqualToString: @"Blastoise"])
    {
        damage = [self damageCalculator:currentEnemyPokemon Pokemon2:currentUserPokemon move:currentEnemyPokemon.move2];;
        NSLog(@"Damage calculated: %f", damage);
        CGFloat willHit = arc4random() % 100;
        enemyMove = currentEnemyPokemon.move2.name;

        if (willHit <= currentEnemyPokemon.move2._acc)
        {
            currentUserPokemon._curHP = currentUserPokemon._curHP - damage;
            userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
            userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            if (currentUserPokemon._curHP <= 0)
            {
                //enemy has fainted!
                userFainted = YES;
                currentUserPokemon.currentStatus = @"FAINTED";
                currentUserPokemon._curHP = 0;
                userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
                userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            }
            [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
            return currentEnemyPokemon.move1.name;
        }
        else{
            enemyMiss = YES;
        }
    }
    else if ([currentEnemyPokemon.name isEqualToString: @"Flareon"] && [currentUserPokemon.name isEqualToString: @"Charizard"])
    {
        damage = [self damageCalculator:currentEnemyPokemon Pokemon2:currentUserPokemon move:currentEnemyPokemon.move2];
        NSLog(@"Damage calculated: %f", damage);
        CGFloat willHit = arc4random() % 100;
        enemyMove = currentEnemyPokemon.move2.name;
        if (willHit <= currentEnemyPokemon.move2._acc)
        {
            currentUserPokemon._curHP = currentUserPokemon._curHP - damage;
            userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
            userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            if (currentUserPokemon._curHP <= 0)
            {
                //enemy has fainted!
                userFainted = YES;
                currentUserPokemon.currentStatus = @"FAINTED";
                currentUserPokemon._curHP = 0;
                userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
                userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];

            }
            [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
            return currentEnemyPokemon.move1.name;
        }
        else{
            enemyMiss = YES;
        }
    }
    else if ([currentEnemyPokemon.name isEqualToString: @"Jolteon"] && [currentUserPokemon.name isEqualToString: @"Charizard"])
    {
        damage = [self damageCalculator:currentEnemyPokemon Pokemon2:currentUserPokemon move:currentEnemyPokemon.move1];
        NSLog(@"Damage calculated: %f", damage);
        CGFloat willHit = arc4random() % 100;
        enemyMove = currentEnemyPokemon.move1.name;
        if (willHit <= currentEnemyPokemon.move1._acc)
        {
            currentUserPokemon._curHP = currentUserPokemon._curHP - damage;
            userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
            userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            if (currentUserPokemon._curHP <= 0)
            {
                //enemy has fainted!
                userFainted = YES;
                currentUserPokemon.currentStatus = @"FAINTED";
                currentUserPokemon._curHP = 0;
                userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
                userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];

            }
            [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
            return currentEnemyPokemon.move1.name;
        }
        else{
            enemyMiss = YES;
        }
    }
    else if ([currentEnemyPokemon.name isEqualToString: @"Jolteon"] && [currentUserPokemon.name isEqualToString: @"Blastoise"])
    {
        damage = [self damageCalculator:currentEnemyPokemon Pokemon2:currentUserPokemon move:currentEnemyPokemon.move1];
        NSLog(@"Damage calculated: %f", damage);
        CGFloat willHit = arc4random() % 100;
        enemyMove = currentEnemyPokemon.move1.name;
        if (willHit <= currentEnemyPokemon.move1._acc)
        {
            currentUserPokemon._curHP = currentUserPokemon._curHP - damage;
            userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
            userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            if (currentUserPokemon._curHP <= 0)
            {
                //enemy has fainted!
                userFainted = YES;
                currentUserPokemon.currentStatus = @"FAINTED";
                currentUserPokemon._curHP = 0;
                userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
                userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];

            }
            [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
            return currentEnemyPokemon.move1.name;
        }
        else{
            enemyMiss = YES;
        }
    }
    else if ([currentEnemyPokemon.name isEqualToString: @"Jolteon"] && [currentUserPokemon.name isEqualToString: @"Venusaur"])
    {
        damage = [self damageCalculator:currentEnemyPokemon Pokemon2:currentUserPokemon move:currentEnemyPokemon.move3];
        NSLog(@"Damage calculated: %f", damage);
        CGFloat willHit = arc4random() % 100;
        enemyMove = currentEnemyPokemon.move3.name;
        if (willHit <= currentEnemyPokemon.move3._acc)
        {
            currentUserPokemon._curHP = currentUserPokemon._curHP - damage;
            userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
            userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
            if (currentUserPokemon._curHP <= 0)
            {
                //enemy has fainted!
                userFainted = YES;
                currentUserPokemon.currentStatus = @"FAINTED";
                currentUserPokemon._curHP = 0;
                userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
                userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];

            }
            [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
            return currentEnemyPokemon.move3.name;
        }
        else{
            enemyMiss = YES;
        }
    }
    
    NSString *nothingHappened = @"Nothing";
    return nothingHappened;
}

-(CGFloat) damageCalculator: (Pokemon *)pokemon1 Pokemon2: (Pokemon*) pokemon2 move: (Move*) moveUsed
{
    NSLog (@"Pokemon attacking: %@", pokemon1.name);
    NSLog (@"Pokemon defending: %@", pokemon2.name);
    
    CGFloat crit = 1;
    int calcCrit = arc4random_uniform(16);
    if (calcCrit == 5)
    {
        crit = 2;
    }
    CGFloat stab = 1;
    CGFloat typeEff = 1;
    if ([pokemon1.type1 isEqual: moveUsed._type ]|| [pokemon1.type2 isEqual: moveUsed._type])
    {
        NSLog(@"Got STAB");
        stab = 1.5;
    }
    
    //Vine Whip vs Vaporeon, Flareon
    if ([moveUsed._type isEqual:@"Grass"] && [pokemon2.type1 isEqual:@"Water"])
    {
        typeEff = 2;
    }
    else if ([moveUsed._type isEqual:@"Grass"] && [pokemon2.type1 isEqual:@"Fire"])
    {
        NSLog(@"Ineffective!");
        typeEff = 0.5;
    }
    
    //Flamethrower vs Vaporeon, Flareon, Venusaur, Blastoise, Charizard
    if ([moveUsed._type isEqual:@"Fire"] && [pokemon2.type1 isEqual:@"Water"])
    {
        typeEff = 0.5;
    }
    else if ([moveUsed._type isEqual:@"Fire"] && [pokemon2.type1 isEqual:@"Fire"])
    {
        typeEff = 0.5;
    }
    else if ([moveUsed._type isEqual:@"Fire"] && [pokemon2.type1 isEqual:@"Grass"])
    {
        typeEff = 2.0;
    }
    
    //Earthquake vs Jolteon, Flareon
    if ([moveUsed._type isEqual:@"Ground"] && [pokemon2.type1 isEqual:@"Fire"])
    {
        typeEff = 2.0;
    }
    else if ([moveUsed._type isEqual:@"Ground"] && [pokemon2.type1 isEqual:@"Electric"])
    {
        typeEff = 2.0;
    }
    
    //Surf vs Flareon, Vaporeon, Charizard, Venusaur, Blastoise
    if ([moveUsed._type isEqual:@"Water"] && [pokemon2.type1 isEqual:@"Fire"])
    {
        NSLog(@"Got super effective");
        typeEff = 2.0;
    }
    else if ([moveUsed._type isEqual:@"Water"] && [pokemon2.type1 isEqual:@"Water"])
    {
        typeEff = 0.5;
    }
    else if ([moveUsed._type isEqual:@"Water"] && [pokemon2.type1 isEqual:@"Grass"])
    {
        typeEff = 0.5;
    }
    
    //Thunderbolt vs Venusaur, Charizard, Blastoise
    if ([moveUsed._type isEqual:@"Electric"] && [pokemon2.type1 isEqual:@"Grass"])
    {
        typeEff = 0.5;
    }
    else if ([moveUsed._type isEqual:@"Electric"] && [pokemon2.type1 isEqual:@"Water"])
    {
        typeEff = 2.0;
    }
    else if ([moveUsed._type isEqual:@"Electric"] && [pokemon2.type2 isEqual:@"Flying"])
    {
        typeEff = 2.0;
    }
    
    CGFloat randomMultiplier = ((CGFloat) (100 - arc4random_uniform(15))/100.0);
    
    CGFloat modifier = stab * typeEff * crit * randomMultiplier;
    NSLog(@"Modifier multiplier: %f", modifier);
    CGFloat damage = 0;
    
    if ([moveUsed._type isEqual:@"Normal"] || [moveUsed._type isEqual:@"Fighting"] || [moveUsed._type isEqual:@"Flying"] || [moveUsed._type isEqual:@"Ground"] || [moveUsed._type isEqual:@"Rock"] || [moveUsed._type isEqual:@"Bug"] || [moveUsed._type isEqual:@"Ghost"] || [moveUsed._type isEqual:@"Poison"])
    {
        damage = ( ((2.0*50.0+10.0)/250.0) * (pokemon1._atk / pokemon2._def) * moveUsed._power + 2) * modifier;
        NSLog(@"%f", (2.0*50.0+10.0)/250.0);
        NSLog(@"%f", ((pokemon1._atk/pokemon2._def)));
        NSLog(@"%f", (CGFloat)moveUsed._power);
        NSLog(@"%f", modifier);
        NSLog(@"Inside damage calculator method damage: %f", damage);
        if (moveUsed._power == 0)
        {
            damage = 0;
        }
        if ([moveUsed.name isEqualToString: @"Dragon Rage"])
        {
            damage = 40;
        }
        if ([moveUsed.name isEqualToString:@"Hyper Beam"])
        {
            cpuSwitched = 1;
        }
    }
    else
    {
        damage = ( ((2.0*50.0+10.0)/250.0) * (pokemon1._special / pokemon2._special) * moveUsed._power + 2) * modifier;
        NSLog(@"%f", (2.0*50.0+10.0)/250.0);
        NSLog(@"%f", ((pokemon1._special/pokemon2._special)));
        NSLog(@"%f", (CGFloat)moveUsed._power);
        NSLog(@"%f", modifier);
        NSLog(@"Inside damage calculator method damage: %f", damage);
        if (moveUsed._power == 0)
        {
            damage = 0;
        }
        if ([moveUsed.name isEqualToString: @"Dragon Rage"])
        {
            damage = 40;
        }
        if ([moveUsed.name isEqualToString:@"Hyper Beam"])
        {
            cpuSwitched = 1;
            NSLog(@"Hyper beam recharge set.");
        }
    }
    return damage;
}

-(void) move1Clicked
{
    NSLog(@"move1Clicked");
    
    userMove = currentUserPokemon.move1.name;

    //Switches always go first!
    [self cpuResponseToWeakness];
    
    CGFloat damage = [self damageCalculator:currentUserPokemon Pokemon2:currentEnemyPokemon move:currentUserPokemon.move1];
    NSLog(@"Damage calculated: %f", damage);
    
    userPokemonBeforeSwitch = currentUserPokemon.name;
    enemyPokemonBeforeSwitch = currentEnemyPokemon.name;
    userMove = currentUserPokemon.move1.name;
    
    CGFloat willHit = arc4random() % 100;
    if (willHit <= currentUserPokemon.move1._acc)
    {
        NSLog(@"Move1 hit: %f", willHit);
        if (currentUserPokemon._speed > currentEnemyPokemon._speed && ![currentUserPokemon.currentStatus isEqualToString: @"FAINTED"])
        {
            userFirst = YES;
            //Go first
            currentEnemyPokemon._curHP = currentEnemyPokemon._curHP - damage;
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
            enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            
            if (currentEnemyPokemon._curHP <= 0)
            {
                enemyFainted = YES;
                //enemy has fainted!
                currentEnemyPokemon.currentStatus = @"FAINTED";
                currentEnemyPokemon._curHP = 0;
                enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
                enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            }
            [self changeHealthBar : enemyPokemonHealth: enemyPokemonHealthBar1 : enemyPokemonHealthBar2 : ( (((CGFloat)currentEnemyPokemon._curHP/(CGFloat)currentEnemyPokemon._maxHP)) * 100.0)];
            
            //AI Turn
            if ([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"])
            {
                enemyAsleep = YES;
                sleepCounter--;
                if (sleepCounter == 0)
                {
                    enemyWoke = YES;
                    currentEnemyPokemon.currentStatus = @"Healthy";
                    enemyPokemonStatus.text = @"Healthy";
                }
            }
            else
            {
                if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] )
                {
                    [self cpuResponse];
                }
                else{cpuSwitched = 0;}
            }
        }
        else
        {
            enemyFirst = YES;
            //AI GOES FIRST
            if ([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"])
            {
                enemyAsleep = YES;
                sleepCounter--;
                if (sleepCounter == 0)
                {
                    enemyWoke = YES;
                    currentEnemyPokemon.currentStatus = @"Healthy";
                    enemyPokemonStatus.text = @"Healthy";

                }
            }
            else
            {
                if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] )
                {
                    [self cpuResponse];

                }
                else{cpuSwitched = 0;}
            }
            
            //Player turn!
            if (![currentUserPokemon.currentStatus isEqualToString: @"FAINTED"])
            {
            currentEnemyPokemon._curHP = currentEnemyPokemon._curHP - damage;
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
            enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
                
            if (currentEnemyPokemon._curHP <= 0)
            {
                //enemy has fainted!
                enemyFainted = YES;
                currentEnemyPokemon.currentStatus = @"FAINTED";
                currentEnemyPokemon._curHP = 0;
                enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
                enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            }

            [self changeHealthBar : enemyPokemonHealth: enemyPokemonHealthBar1 : enemyPokemonHealthBar2 : ( (((CGFloat)currentEnemyPokemon._curHP/(CGFloat)currentEnemyPokemon._maxHP)) * 100.0)];
            }
        }
    }
    else{
        NSLog(@"Move1 miss: %f", willHit);
        //Dialog says you missed
        userMiss = YES;
        if ([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"])
        {
            enemyAsleep = YES;
            sleepCounter--;
            if (sleepCounter == 0)
            {
                enemyWoke = YES;
                currentEnemyPokemon.currentStatus = @"Healthy";
                enemyPokemonStatus.text = @"Healthy";

            }
        }
        else
        {
            if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] )
            {
                [self cpuResponse];
            }
            else{cpuSwitched = 0;}
        }
    }
    [self currentAttackDialogue];
    [self resetDialogueFields];
}

-(void) move2Clicked
{
    //Switches always go first!
    [self cpuResponseToWeakness];
    userMove = currentUserPokemon.move2.name;

    NSLog(@"move2Clicked");
    CGFloat damage = [self damageCalculator:currentUserPokemon Pokemon2:currentEnemyPokemon move:currentUserPokemon.move2];
    NSLog(@"Damage calculated: %f", damage);
    
    CGFloat willHit = arc4random() % 100;
    if (willHit <= currentUserPokemon.move2._acc)
    {
        NSLog(@"Move2 hit: %f", willHit);
        if (currentUserPokemon._speed > currentEnemyPokemon._speed && ![currentUserPokemon.currentStatus isEqualToString: @"FAINTED"])
        {
            userFirst = YES;
            //Go first
            currentEnemyPokemon._curHP = currentEnemyPokemon._curHP - damage;
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
            enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            if (currentEnemyPokemon._curHP <= 0)
            {
                //enemy has fainted!
                enemyFainted = YES;
                currentEnemyPokemon.currentStatus = @"FAINTED";
                currentEnemyPokemon._curHP = 0;
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
                enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];


            }
            [self changeHealthBar : enemyPokemonHealth: enemyPokemonHealthBar1 : enemyPokemonHealthBar2 : ( (((CGFloat)currentEnemyPokemon._curHP/(CGFloat)currentEnemyPokemon._maxHP)) * 100.0)];
            
            //AI TURN
            if ([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"])
            {
                enemyAsleep = YES;
                sleepCounter--;
                if (sleepCounter == 0)
                {
                    enemyWoke = YES;
                    currentEnemyPokemon.currentStatus = @"Healthy";
                    enemyPokemonStatus.text = @"Healthy";

                }
            }
            else
            {
                if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] )
                {
                   [self cpuResponse];
  
                }
                else{cpuSwitched = 0;}
            }
        }
        else
        {
            enemyFirst = YES;
            //AI GOES FIRST
            if ([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"])
            {
                enemyAsleep = YES;
                sleepCounter--;
                if (sleepCounter == 0)
                {
                    enemyWoke = YES;
                    currentEnemyPokemon.currentStatus = @"Healthy";
                    enemyPokemonStatus.text = @"Healthy";

                }
            }
            else
            {
                if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] )
                {
                    [self cpuResponse];
                }
                else{cpuSwitched = 0;}
            }
            
            
            //PLAYER TURN
            if (![currentUserPokemon.currentStatus isEqualToString: @"FAINTED"])
            {
                currentEnemyPokemon._curHP = currentEnemyPokemon._curHP - damage;
                enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
                enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
                if (currentEnemyPokemon._curHP <= 0)
                {
                    //enemy has fainted!
                    enemyFainted = YES;
                    currentEnemyPokemon.currentStatus = @"FAINTED";
                    currentEnemyPokemon._curHP = 0;
                    enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
                    enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
                }
                [self changeHealthBar : enemyPokemonHealth: enemyPokemonHealthBar1 : enemyPokemonHealthBar2 : ( (((CGFloat)currentEnemyPokemon._curHP/(CGFloat)currentEnemyPokemon._maxHP)) * 100.0)];
            }
        }
    }
    else{
        NSLog(@"Move2 miss: %f", willHit);
        //Dialog says you missed
        userMiss = YES;
        if ([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"])
        {
            enemyAsleep = YES;
            sleepCounter--;
            if (sleepCounter == 0)
            {
                enemyWoke = YES;
                currentEnemyPokemon.currentStatus = @"Healthy";
                enemyPokemonStatus.text = @"Healthy";

            }
        }
        else
        {
            if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] )
            {
                [self cpuResponse];

            }
            else{cpuSwitched = 0;}
        }

    }
    [self currentAttackDialogue];
    [self resetDialogueFields];
}

-(void) move3Clicked
{
    //Switches always go first!
    [self cpuResponseToWeakness];
    CGFloat damage = [self damageCalculator:currentUserPokemon Pokemon2:currentEnemyPokemon move:currentUserPokemon.move3];
    userMove = currentUserPokemon.move3.name;
    NSLog(@"Damage calculated: %f", damage);
    
    CGFloat willHit = arc4random() % 100;
    if (willHit <= currentUserPokemon.move3._acc)
    {
        NSLog(@"Move3 hit: %f", willHit);
        if (currentUserPokemon._speed > currentEnemyPokemon._speed && ![currentUserPokemon.currentStatus isEqualToString: @"FAINTED"])
        {
            userFirst = YES;
            //Go first
            currentEnemyPokemon._curHP = currentEnemyPokemon._curHP - damage;
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
            enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            if (currentEnemyPokemon._curHP <= 0)
            {
                //enemy has fainted!
                enemyFainted = YES;
                currentEnemyPokemon.currentStatus = @"FAINTED";
                currentEnemyPokemon._curHP = 0;
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
                enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            }
            if ([currentUserPokemon.move3.name isEqualToString: @"Sleep Powder"] && [currentEnemyPokemon.currentStatus isEqualToString: @"Healthy"])
            {
                enemyAsleep = YES;
                currentEnemyPokemon.currentStatus = @"Sleeping";
                enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
                sleepCounter = (arc4random() % 3) + 2;
            }
            else if ([currentUserPokemon.move3.name isEqualToString: @"Sleep Powder"] && ![currentEnemyPokemon.currentStatus isEqualToString: @"Healthy"])
            {
                enemyAsleep = YES;
                NSLog(@"Enemy already has a status other than healthy.");
            }
            
            [self changeHealthBar : enemyPokemonHealth: enemyPokemonHealthBar1 : enemyPokemonHealthBar2 : ( (((CGFloat)currentEnemyPokemon._curHP/(CGFloat)currentEnemyPokemon._maxHP)) * 100.0)];
            
            //AI TURN
            if ([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"])
            {
                enemyAsleep = YES;
                sleepCounter--;
                if (sleepCounter == 0)
                {
                    enemyWoke = YES;
                    currentEnemyPokemon.currentStatus = @"Healthy";
                    enemyPokemonStatus.text = @"Healthy";

                }
            }
            else
            {
                if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] )
                {
                   [self cpuResponse];

                }
                else{cpuSwitched = 0;}
            }
        }
        else
        {
            enemyFirst = YES;
            //AI GOES FIRST
            if ([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"])
            {
                enemyAsleep = YES;
                sleepCounter--;
                if (sleepCounter == 0)
                {
                    enemyWoke = YES;
                    currentEnemyPokemon.currentStatus = @"Healthy";
                    enemyPokemonStatus.text = @"Healthy";

                }
            }
            else
            {
                if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] )
                {
                    [self cpuResponse];
                }
                else{cpuSwitched = 0;}
            }
            
            
            
            
            //PLAYER TURN
          
            if (![currentUserPokemon.currentStatus isEqualToString: @"FAINTED"]){
            currentEnemyPokemon._curHP = currentEnemyPokemon._curHP - damage;
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
            enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            if (currentEnemyPokemon._curHP <= 0)
            {
                //enemy has fainted!
                enemyFainted = YES;
                currentEnemyPokemon.currentStatus = @"FAINTED";
                currentEnemyPokemon._curHP = 0;
                enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
                enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
                
            }

            if ([currentUserPokemon.move3.name isEqualToString: @"Sleep Powder"] && [currentEnemyPokemon.currentStatus isEqualToString: @"Healthy"])
            {
                enemyAsleep = YES;
                currentEnemyPokemon.currentStatus = @"Sleeping";
                enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
                sleepCounter = (arc4random() % 3) + 2;
            }
            else if ([currentUserPokemon.move3.name isEqualToString: @"Sleep Powder"] && ![currentEnemyPokemon.currentStatus isEqualToString: @"Healthy"])
            {
                enemyAsleep = YES;
                NSLog(@"Enemy already has a status other than healthy.");
            }
            [self changeHealthBar : enemyPokemonHealth: enemyPokemonHealthBar1 : enemyPokemonHealthBar2 : ( (((CGFloat)currentEnemyPokemon._curHP/(CGFloat)currentEnemyPokemon._maxHP)) * 100.0)];
            }
        }
    }
    else{
        NSLog(@"Move3 miss: %f", willHit);
        //Dialog says you missed
        userMiss = YES;
        if ([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"])
        {
            enemyAsleep = YES;
            sleepCounter--;
            if (sleepCounter == 0)
            {
                enemyWoke = YES;
                currentEnemyPokemon.currentStatus = @"Healthy";
                enemyPokemonStatus.text = @"Healthy";

            }
        }
        else
        {
            if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] )
            {
                [self cpuResponse];
    
            }
            else{cpuSwitched = 0;}
        }
    }
    [self currentAttackDialogue];
    [self resetDialogueFields];
}

-(void) move4Clicked
{
    //Switches always go first!
    [self cpuResponseToWeakness];
    
    CGFloat damage = [self damageCalculator:currentUserPokemon Pokemon2:currentEnemyPokemon move:currentUserPokemon.move4];
    NSLog(@"Damage calculated: %f", damage);
    
    CGFloat willHit = arc4random() % 100;
    userMove = currentUserPokemon.move4.name;
    if (willHit <= currentUserPokemon.move4._acc)
    {
        NSLog(@"Move4 hit: %f", willHit);
        if (currentUserPokemon._speed > currentEnemyPokemon._speed && ![currentUserPokemon.currentStatus isEqualToString: @"FAINTED"])
        {
            NSLog(@"%@ Speed: %d", currentUserPokemon.name, currentUserPokemon._speed);
            NSLog(@"%@ Speed: %d", currentEnemyPokemon.name, currentEnemyPokemon._speed);
            userFirst = YES;
            //Go first
            currentEnemyPokemon._curHP = currentEnemyPokemon._curHP - damage;
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
            enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            if (currentEnemyPokemon._curHP <= 0)
            {
                //enemy has fainted!
                enemyFainted = YES;
                currentEnemyPokemon.currentStatus = @"FAINTED";
                currentEnemyPokemon._curHP = 0;
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
                enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            }
            /*
            if ([currentUserPokemon.move4.name isEqualToString: @"Poison Powder"] && [currentEnemyPokemon.currentStatus isEqualToString: @"Healthy"])
            {
                currentEnemyPokemon.currentStatus = @"Poisoned";
                enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            }
            else if ([currentUserPokemon.move4.name isEqualToString: @"Poison Powder"] && ![currentEnemyPokemon.currentStatus isEqualToString: @"Healthy"])
            {
                NSLog(@"Enemy already has a status other than healthy.");
            }*/
            [self changeHealthBar : enemyPokemonHealth: enemyPokemonHealthBar1 : enemyPokemonHealthBar2 : ( (((CGFloat)currentEnemyPokemon._curHP/(CGFloat)currentEnemyPokemon._maxHP)) * 100.0)];
            
            //AI TURN
            if ([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"])
            {
                enemyAsleep = YES;
                sleepCounter--;
                if (sleepCounter == 0)
                {
                    enemyWoke = YES;
                    currentEnemyPokemon.currentStatus = @"Healthy";
                    enemyPokemonStatus.text = @"Healthy";

                }
            }
            else
            {
                if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] )
                {
                    [self cpuResponse];
      
                }
                else{cpuSwitched = 0;}
            }
        }
        else
        {
            enemyFirst = YES;
            //AI GOES FIRST
            if ([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"])
            {
                enemyAsleep = YES;
                sleepCounter--;
                if (sleepCounter == 0)
                {
                    enemyWoke = YES;
                    currentEnemyPokemon.currentStatus = @"Healthy";
                    enemyPokemonStatus.text = @"Healthy";

                }
            }
            else
            {
                if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] )
                {
                    [self cpuResponse];
                }
               else{cpuSwitched = 0;}
            }
            
            //PLAYER TURN
            if (![currentUserPokemon.currentStatus isEqualToString: @"FAINTED"]){
            currentEnemyPokemon._curHP = currentEnemyPokemon._curHP - damage;
            enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
            enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            if (currentEnemyPokemon._curHP <= 0)
            {
                //enemy has fainted!
                enemyFainted = YES;
                currentEnemyPokemon.currentStatus = @"FAINTED";
                currentEnemyPokemon._curHP = 0;
                enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
                enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            }
    /*
            if ([currentUserPokemon.move4.name isEqualToString: @"Poison Powder"] && [currentEnemyPokemon.currentStatus isEqualToString: @"Healthy"])
            {
                currentEnemyPokemon.currentStatus = @"Poisoned";
                enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
            }
            else if ([currentUserPokemon.move4.name isEqualToString: @"Poison Powder"] && ![currentEnemyPokemon.currentStatus isEqualToString: @"Healthy"])
            {
                NSLog(@"Enemy already has a status other than healthy.");
            }*/
            [self changeHealthBar : enemyPokemonHealth: enemyPokemonHealthBar1 : enemyPokemonHealthBar2 : ( (((CGFloat)currentEnemyPokemon._curHP/(CGFloat)currentEnemyPokemon._maxHP)) * 100.0)];
            }
        }
    }
    else{
        NSLog(@"Move4 miss: %f", willHit);
        //Dialog says you missed
        userMiss = YES;
        if ([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"])
        {
            enemyAsleep = YES;
            sleepCounter--;
            if (sleepCounter == 0)
            {
                enemyWoke = YES;
                currentEnemyPokemon.currentStatus = @"Healthy";
                enemyPokemonStatus.text = @"Healthy";

            }
        }
        else
        {
            if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] )
            {
                [self cpuResponse];
 
            }
            else{cpuSwitched = 0;}
        }

    }
    [self currentAttackDialogue];
    [self resetDialogueFields];
}


//Switch currentPokemon
-(void) switch1Clicked
{
    userPokemonBeforeSwitch = currentUserPokemon.name;
    userSwitched = YES;
    if([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"]){
        enemyAsleep = YES;
    }
    [self removeAllChildren];
    if ([currentUserPokemon.currentStatus isEqualToString:@"FAINTED"])
    {
        cpuSwitched = 1;
    }
    currentUserPokemon = userPokemon1;
    
    NSLog(@"SwitchButton1 clicked");
    currentUserPokemon.image = userPokemon1.image;
    
    [self scaleImage:currentUserPokemon];
    [self setInitialUserPosition:currentUserPokemon];
    [self setInitialEnemyPosition:currentEnemyPokemon];
    
    userPokemonName.text = [NSString stringWithFormat: @"%@", currentUserPokemon.name];
    userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
    userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
    
    [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
    
    
    [move1 setTitle: currentUserPokemon.move1.name forState: UIControlStateNormal];
    
    [move2 setTitle: currentUserPokemon.move2.name forState: UIControlStateNormal];
    
    [move3 setTitle: currentUserPokemon.move3.name forState: UIControlStateNormal];
    
    [move4 setTitle: currentUserPokemon.move4.name forState: UIControlStateNormal];
    [self addChild: currentUserPokemon.image];
    [self addChild: currentEnemyPokemon.image];
    
    switchFlag = 1;
    
    if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] &&  ![currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"] )
    {
        [self cpuResponse];
    }
    cpuSwitched = 0;
    [self currentAttackDialogue];
    [self resetDialogueFields];
}

-(void) switch2Clicked
{
    userPokemonBeforeSwitch = currentUserPokemon.name;
    userSwitched = YES;
    [self removeAllChildren];
    if([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"]){
        enemyAsleep = YES;
    }
    if ([currentUserPokemon.currentStatus isEqualToString:@"FAINTED"])
    {
        cpuSwitched = 1;
    }
    currentUserPokemon = userPokemon2;
    
    NSLog(@"SwitchButton2 clicked");
    currentUserPokemon.image = userPokemon2.image;
    
    [self scaleImage:currentUserPokemon];
    [self setInitialUserPosition:currentUserPokemon];
    [self setInitialEnemyPosition:currentEnemyPokemon];
    
    userPokemonName.text = [NSString stringWithFormat: @"%@", currentUserPokemon.name];
    userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
    userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
    
    [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
    
    [move1 setTitle: currentUserPokemon.move1.name forState: UIControlStateNormal];
    
    [move2 setTitle: currentUserPokemon.move2.name forState: UIControlStateNormal];
    
    [move3 setTitle: currentUserPokemon.move3.name forState: UIControlStateNormal];
    
    [move4 setTitle: currentUserPokemon.move4.name forState: UIControlStateNormal];
    
    [self addChild: currentUserPokemon.image];
    [self addChild: currentEnemyPokemon.image];
    
    switchFlag = 1;
    
    if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] &&  ![currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"] )
    {
        [self cpuResponse];
    }
    cpuSwitched = 0;
    [self currentAttackDialogue];
}

-(void) switch3Clicked
{
    userPokemonBeforeSwitch = currentUserPokemon.name;
    userSwitched = YES;
    [self removeAllChildren];
    if([currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"]){
        enemyAsleep = YES;
    }
    if ([currentUserPokemon.currentStatus isEqualToString:@"FAINTED"])
    {
        cpuSwitched = 1;
    }
    currentUserPokemon = userPokemon3;
    
    NSLog(@"SwitchButton3 clicked");
    currentUserPokemon.image = userPokemon3.image;
    
    [self scaleImage:currentUserPokemon];
    [self setInitialUserPosition:currentUserPokemon];
    [self setInitialEnemyPosition:currentEnemyPokemon];
    
    userPokemonName.text = [NSString stringWithFormat: @"%@", currentUserPokemon.name];
    userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
    userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
    
    [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
    
    [move1 setTitle: currentUserPokemon.move1.name forState: UIControlStateNormal];
    
    [move2 setTitle: currentUserPokemon.move2.name forState: UIControlStateNormal];
    
    [move3 setTitle: currentUserPokemon.move3.name forState: UIControlStateNormal];
    
    [move4 setTitle: currentUserPokemon.move4.name forState: UIControlStateNormal];
    [self addChild: currentUserPokemon.image];
    [self addChild: currentEnemyPokemon.image];
    
    switchFlag = 1;
    
    if (!(cpuSwitched == 1) && ![currentEnemyPokemon.currentStatus isEqualToString:@"FAINTED"] &&  ![currentEnemyPokemon.currentStatus isEqualToString:@"Sleeping"])
    {
        [self cpuResponse];
    }
    cpuSwitched = 0;
    [self currentAttackDialogue];
    [self resetDialogueFields];
}

-(void) enemySwitch: (Pokemon *)enemyPokemon
{
    enemyPokemonBeforeSwitch = currentEnemyPokemon.name;
    enemySwitched = YES;
    currentEnemyPokemon = enemyPokemon;
    [self removeAllChildren];
    
    
    NSLog(@"Enemy switch clicked");
    currentEnemyPokemon.image = enemyPokemon.image;
    
    [self scaleImage:currentEnemyPokemon];
    [self setInitialUserPosition:currentUserPokemon];
    [self setInitialEnemyPosition:currentEnemyPokemon];
    
    enemyPokemonName.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.name];
    enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
    enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
    
    [self changeHealthBar : enemyPokemonHealth: enemyPokemonHealthBar1 : enemyPokemonHealthBar2 : ( (((CGFloat)currentEnemyPokemon._curHP/(CGFloat)currentEnemyPokemon._maxHP)) * 100.0)];
    
    
    [self addChild: currentUserPokemon.image];
    [self addChild: currentEnemyPokemon.image];
    [self resetDialogueFields];
}


- (void) changeHealthBar: (UILabel *) currentPokemonHealthBar: (UILabel *) currentPokemonHealthBar1 : (UILabel *) currentPokemonHealthBar2 : (CGFloat) percentage{
    
    currentPokemonHealthBar1.frame = CGRectMake(currentPokemonHealthBar1.frame.origin.x, currentPokemonHealthBar1.frame.origin.y, (currentPokemonHealthBar.frame.size.width)  * (percentage/100.0), currentPokemonHealthBar1.frame.size.height);
    currentPokemonHealthBar1.layer.borderColor = [UIColor blackColor].CGColor;
    currentPokemonHealthBar1.layer.borderWidth = 1.0F;
    if(percentage >= 50){
        currentPokemonHealthBar1.backgroundColor = [UIColor greenColor];
    }
    else if(percentage >= 25){
      currentPokemonHealthBar1.backgroundColor = [UIColor yellowColor];
    }
    else{
        currentPokemonHealthBar1.backgroundColor = [UIColor redColor];
    }
    
    currentPokemonHealthBar2.frame = CGRectMake(CGRectGetMaxX(currentPokemonHealthBar1.frame), currentPokemonHealthBar1.frame.origin.y, (currentPokemonHealthBar.frame.size.width)  * (1 - percentage/100.0), currentPokemonHealthBar1.frame.size.height);
    currentPokemonHealthBar2.layer.borderColor = [UIColor blackColor].CGColor;
    currentPokemonHealthBar2.layer.borderWidth = 1.0F;
    currentPokemonHealthBar2.backgroundColor = [UIColor whiteColor];
}

- (void) setInitialUserPosition: (Pokemon *) userPokemon{
    userPokemon.image.position = CGPointMake(CGRectGetMidX(userPokemonBox.frame), gameScreenHeight - CGRectGetMidY(userPokemonBox.frame));
    
}

- (void) setInitialEnemyPosition: (Pokemon *) enemyPokemon{
    enemyPokemon.image.position = CGPointMake(CGRectGetMidX(enemyPokemonBox.frame), gameScreenHeight - CGRectGetMidY(enemyPokemonBox.frame));
}

- (void) scaleImage: (Pokemon *)currentPokemon{
    float scaleFactor = 1.0;
    
    while((currentPokemon.image.frame.size.width * scaleFactor < userPokemonBox.frame.size.width) && (currentPokemon.image.frame.size.height * scaleFactor < userPokemonBox.frame.size.height)){
        scaleFactor++;
    }
    //this means no resizing needed
    if(scaleFactor != 1){
        currentPokemon.image.xScale = scaleFactor;
        currentPokemon.image.yScale = scaleFactor;
    }
}

-(void) resetGame
{
    [self removeAllChildren];
    
    pokemon1.backgroundColor = [UIColor whiteColor];
    pokemon2.backgroundColor = [UIColor whiteColor];
    pokemon3.backgroundColor = [UIColor whiteColor];

    enemyPokemon1._curHP = enemyPokemon1._maxHP;
    enemyPokemon2._curHP = enemyPokemon2._maxHP;
    enemyPokemon3._curHP = enemyPokemon3._maxHP;
    
    userPokemon1._curHP = userPokemon1._maxHP;
    userPokemon2._curHP = userPokemon2._maxHP;
    userPokemon3._curHP = userPokemon3._maxHP;
    
    enemyPokemon1.currentStatus = @"Healthy";
    enemyPokemon2.currentStatus = @"Healthy";
    enemyPokemon3.currentStatus = @"Healthy";
    
    userPokemon1.currentStatus = @"Healthy";
    userPokemon2.currentStatus = @"Healthy";
    userPokemon3.currentStatus = @"Healthy";
    
    [self changeHealthBar : enemyPokemonHealth: enemyPokemonHealthBar1 : enemyPokemonHealthBar2 : ( (((CGFloat)currentEnemyPokemon._curHP/(CGFloat)currentEnemyPokemon._maxHP)) * 100.0)];
    [self changeHealthBar : userPokemonHealth: userPokemonHealthBar1 : userPokemonHealthBar2 : ( (((CGFloat)currentUserPokemon._curHP/(CGFloat)currentUserPokemon._maxHP)) * 100.0)];
    
    currentUserPokemon = userPokemon1;
    currentEnemyPokemon = enemyPokemon1;
    
    currentUserPokemon.image = userPokemon1.image;
    currentEnemyPokemon.image = enemyPokemon1.image;
    
    [self scaleImage:currentUserPokemon];
    [self setInitialUserPosition:currentUserPokemon];
    [self setInitialEnemyPosition:currentEnemyPokemon];
    
    userPokemonName.text = [NSString stringWithFormat: @"%@", currentUserPokemon.name];
    userPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentUserPokemon._curHP, currentUserPokemon._maxHP];
    userPokemonStatus.text = [NSString stringWithFormat: @"%@", currentUserPokemon.currentStatus];
    
    enemyPokemonName.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.name];
    enemyPokemonHealthValue.text = [NSString stringWithFormat: @"%d/%d", currentEnemyPokemon._curHP, currentEnemyPokemon._maxHP];
    enemyPokemonStatus.text = [NSString stringWithFormat: @"%@", currentEnemyPokemon.currentStatus];
    
    [move1 setTitle: currentUserPokemon.move1.name forState: UIControlStateNormal];
    
    [move2 setTitle: currentUserPokemon.move2.name forState: UIControlStateNormal];
    
    [move3 setTitle: currentUserPokemon.move3.name forState: UIControlStateNormal];
    
    [move4 setTitle: currentUserPokemon.move4.name forState: UIControlStateNormal];
    [self addChild: currentUserPokemon.image];
    [self addChild: currentEnemyPokemon.image];
    
    [self resetDialogueFields];

}

- (void) currentAttackDialogue{
    
    [self hideMoves];
    [moveBox setHidden: NO];
    [self disableSwitches];

    NSString *currentAction;

    
    //USER SWITCHED
    if(userSwitched == YES){
        NSLog(@"USER SWITCHED");
        if(enemyWoke == YES){
            //USER SWITCHES, ENEMY WOKE UP
            currentAction = [NSString stringWithFormat:@"%@ has been switched in. \n\n %@ woke up.",currentUserPokemon.name, currentEnemyPokemon.name];
        }
        else if(enemyAsleep == YES){
            //USER SWITCHES, ENEMY ASLEEP
             currentAction = [NSString stringWithFormat:@"%@ has been switched in. \n\n %@ is fast asleep.",currentUserPokemon.name, currentEnemyPokemon.name];
        }
        else{
            //USER SWITCHES, ENEMY MISSES
            if(enemyMiss == YES){
                currentAction = [NSString stringWithFormat:@"%@ has been switched in. \n\n %@ uses %@. \n %@ missed.",currentUserPokemon.name, currentEnemyPokemon.name,enemyMove,currentEnemyPokemon.name];
                
            }
            else{
                //USER SWITCHES, ENEMY HITS, USER FAINTS
                if(userFainted == YES){
                    currentAction = [NSString stringWithFormat:@"%@ has been switched in. \n\n %@ uses %@. \n %@ has fainted.",currentUserPokemon.name, currentEnemyPokemon.name,enemyMove,currentUserPokemon.name];
                    
                }
                //USER SWITCHES, ENEMY HITS, NO FAINT
                else{
                    currentAction = [NSString stringWithFormat:@"%@ has been switched in. \n\n %@ uses %@.",currentUserPokemon.name, currentEnemyPokemon.name,enemyMove];
                }
            }
        }
    }
    //AI SWITCHED
    else if(enemySwitched == YES){
        //ENEMY SWITCHES, USER MISSES
        if(userMiss == YES){
            currentAction = [NSString stringWithFormat:@"%@ has been switched in. \n\n %@ uses %@. \n %@ missed.",currentEnemyPokemon.name, currentUserPokemon.name,userMove,currentUserPokemon.name];
        }
        else{
            //ENEMY SWITCHES, USER HITS, ENEMY FAINTS
            if(enemyFainted == YES){
                currentAction = [NSString stringWithFormat:@"%@ has been switched in. \n\n %@ uses %@. \n %@ has fainted.",currentEnemyPokemon.name, currentUserPokemon.name,userMove,currentEnemyPokemon.name];
                
            }
            //ENEMY SWITCHES, USER HITS, NO FAINT
            else{
                currentAction = [NSString stringWithFormat:@"%@ has been switched in. \n\n %@ uses %@.",currentEnemyPokemon.name, currentUserPokemon.name,userMove];
            }
        }
        NSLog(@"AI SWITCHED");
    }
    //NO ONE SWITCHED
    else{
        if(userFirst == YES){
            if(enemyWoke == YES){
                if(userMiss == YES){
                    //NO SWITCHES,USER FIRST, NO FAINT, ENEMY WOKE UP, USER MISS
                    currentAction = [NSString stringWithFormat:@"%@ used %@.\n %@ missed. \n \n %@ woke up.",currentUserPokemon.name, userMove,currentUserPokemon.name, currentEnemyPokemon.name];
                }
                else{
                    if(enemyFainted == YES){
                        //NO SWITCHES,USER FIRST, ENEMY FAINT, ENEMY WOKE UP, USER HIT
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n \n %@ has fainted.",currentUserPokemon.name, userMove, currentEnemyPokemon.name];
                    }
                    else{
                        //NO SWITCHES,USER FIRST, NO FAINT, ENEMY WOKE UP, USER HIT
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n \n %@ woke up.",currentUserPokemon.name, userMove, currentEnemyPokemon.name];
                    }
                }
            }
            else if(enemyAsleep == YES){
                if(userMiss == YES){
                    //NO SWITCHES,USER FIRST, NO FAINT, ENEMY ASLEEP, USER MISS
                    currentAction = [NSString stringWithFormat:@"%@ used %@.\n %@ missed. \n \n %@ is fast asleep.",currentUserPokemon.name, userMove,currentUserPokemon.name, currentEnemyPokemon.name];
                }
                else{
                    if(enemyFainted == YES){
                        //NO SWITCHES,USER FIRST, ENEMY FAINT, ENEMY ASLEEP, USER HIT
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n \n %@ has fainted. \n",currentUserPokemon.name, userMove, currentEnemyPokemon.name];
                    }
                    else{
                        //NO SWITCHES,USER FIRST, NO FAINT, ENEMY ASLEEP, USER HIT
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n \n %@ is fast asleep.",currentUserPokemon.name, userMove, currentEnemyPokemon.name];
                    }
                }
            }
            else{
                if(userMiss == YES && enemyMiss == YES){
                    //NO SWITCHES,USER FIRST, NO FAINT, ENEMY AWAKE, USER MISS, ENEMY MISS
                    currentAction = [NSString stringWithFormat:@"%@ used %@.\n %@ missed. \n \n %@ used %@. \n %@ missed.",currentUserPokemon.name, userMove,currentUserPokemon.name, currentEnemyPokemon.name, enemyMove, currentEnemyPokemon.name];
                    
                }
                else if(userMiss == YES){
                    if(userFainted == YES){
                        //NO SWITCHES,USER FIRST, USER FAINT, ENEMY AWAKE, USER MISS, ENEMY HIT
                        currentAction = [NSString stringWithFormat:@"%@ used %@.\n %@ missed. \n \n %@ used %@. \n \n %@ has fainted.",currentUserPokemon.name, userMove,currentUserPokemon.name, currentEnemyPokemon.name, enemyMove, currentUserPokemon.name];
                    }
                    else{
                        //NO SWITCHES,USER FIRST, NO FAINT, ENEMY AWAKE, USER MISS, ENEMY HIT
                        currentAction = [NSString stringWithFormat:@"%@ used %@.\n %@ missed. \n \n %@ used %@.",currentUserPokemon.name, userMove,currentUserPokemon.name, currentEnemyPokemon.name, enemyMove];
                    }
                }
                else if(enemyMiss == YES){
                    if(enemyFainted == YES){
                        //NO SWITCHES,USER FIRST, ENEMY FAINT, ENEMY AWAKE, USER HIT, ENEMY MISS
                        currentAction = [NSString stringWithFormat:@"%@ used %@.\n \n %@ has fainted. ",currentUserPokemon.name, userMove, currentEnemyPokemon.name];
                    }
                    else{
                        //NO SWITCHES,USER FIRST, NO FAINT, ENEMY AWAKE, USER HIT, ENEMY MISS
                        currentAction = [NSString stringWithFormat:@"%@ used %@.\n \n %@ used %@. %@ missed. ",currentUserPokemon.name, userMove,currentEnemyPokemon.name, enemyMove,currentEnemyPokemon.name];
                    }
                }
                else{
                    if(enemyFainted == YES){
                        //NO SWITCHES,USER FIRST, ENEMY FAINT, ENEMY AWAKE, USER HIT, ENEMY HIT
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n \n %@ has fainted.",currentUserPokemon.name, userMove, currentEnemyPokemon.name];
                    }
                    else if(userFainted == YES){
                        //NO SWITCHES,USER FIRST, USER FAINT, ENEMY AWAKE, USER HIT, ENEMY HIT
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n \n %@ used %@. \n \n %@ has fainted.",currentUserPokemon.name, userMove,currentEnemyPokemon.name, enemyMove,currentUserPokemon.name];
                    }
                    else{
                        //NO SWITCHES,USER FIRST, NO FAINT, ENEMY AWAKE, USER HIT, ENEMY HIT
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n \n %@ used %@.",currentUserPokemon.name, userMove,currentEnemyPokemon.name, enemyMove];
                    }
                }
                
            }
        }
        else{
            if(enemyWoke == YES){
                if(userMiss == YES){
                    //NO SWITCHES,ENEMY FIRST, NO FAINT, ENEMY WOKE UP, USER MISS
                    currentAction = [NSString stringWithFormat:@"%@ woke up. \n \n %@ used %@.\n %@ missed.",currentEnemyPokemon.name, currentUserPokemon.name, userMove, currentUserPokemon.name];
                }
                else{
                    if(enemyFainted == YES){
                        //NO SWITCHES,ENEMY FIRST, ENEMY FAINT, ENEMY WOKE UP, USER HIT
                        currentAction = [NSString stringWithFormat:@"%@ woke up. \n \n %@ used %@. \n \n %@ has fainted." ,currentEnemyPokemon.name, currentUserPokemon.name, userMove,  currentEnemyPokemon.name];
                    }
                    else{
                        //NO SWITCHES,ENEMY FIRST, NO FAINT, ENEMY WOKE UP, USER HIT
                        currentAction = [NSString stringWithFormat:@"%@ woke up. \n \n %@ used %@." ,currentEnemyPokemon.name, currentUserPokemon.name, userMove];
                    }
                }
            }
            else if(enemyAsleep == YES){
                if(userMiss == YES){
                    //NO SWITCHES,ENEMY FIRST, NO FAINT, ENEMY ASLEEP, USER MISS
                    currentAction = [NSString stringWithFormat:@"%@ is fast asleep. \n \n %@ used %@.\n %@ missed.",currentEnemyPokemon.name, currentUserPokemon.name,userMove, currentUserPokemon.name];
                }
                else{
                    if(enemyFainted == YES){
                        //NO SWITCHES,ENEMY FIRST, ENEMY FAINT, ENEMY ASLEEP, USER HIT
                        currentAction = [NSString stringWithFormat:@"%@ is fast asleep. \n \n %@ used %@. \n \n %@ has fainted. ",currentEnemyPokemon.name, currentUserPokemon.name,userMove,  currentEnemyPokemon.name];
                    }
                    else{
                        //NO SWITCHES,ENEMY FIRST, NO FAINT, ENEMY ASLEEP, USER HIT
                        //JOLTEON SPECIFIC CASE ONLY
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n \n %@ used %@. \n %@ is fast asleep.",currentEnemyPokemon.name, enemyMove, currentUserPokemon.name,userMove, currentEnemyPokemon.name];
                    }
                }
            }
            else{
                if(userMiss == YES && enemyMiss == YES){
                    //NO SWITCHES,ENEMY FIRST, NO FAINT, ENEMY AWAKE, USER MISS, ENEMY MISS
                    currentAction = [NSString stringWithFormat:@"%@ used %@. \n %@ missed \n \n %@ used %@.\n %@ missed.",currentEnemyPokemon.name, enemyMove, currentEnemyPokemon.name, currentUserPokemon.name, userMove,currentUserPokemon.name];
                    
                }
                else if(userMiss == YES){
                    if(userFainted == YES){
                        //NO SWITCHES,ENEMY FIRST, USER FAINT, ENEMY AWAKE, USER MISS, ENEMY HIT
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n \n %@ has fainted.",currentEnemyPokemon.name, enemyMove, currentUserPokemon.name];
                    }
                    else{
                        //NO SWITCHES,ENEMY FIRST, NO FAINT, ENEMY AWAKE, USER MISS, ENEMY HIT
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n \n %@ used %@.\n %@ missed.",currentEnemyPokemon.name, enemyMove, currentUserPokemon.name, userMove,currentUserPokemon.name];
                    }
                }
                else if(enemyMiss == YES){
                    if(enemyFainted == YES){
                        //NO SWITCHES,ENEMY FIRST, ENEMY FAINT, ENEMY AWAKE, USER HIT, ENEMY MISS
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n %@ missed.\n \n %@ used %@. \n\n %@ has fainted. ",currentEnemyPokemon.name, enemyMove,currentEnemyPokemon.name, currentUserPokemon.name, userMove, currentEnemyPokemon];
                    }
                    else{
                        //NO SWITCHES,ENEMY FIRST, NO FAINT, ENEMY AWAKE, USER HIT, ENEMY MISS
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n %@ missed.\n \n %@ used %@.",currentEnemyPokemon.name, enemyMove,currentEnemyPokemon.name, currentUserPokemon.name, userMove];
                    }

                }
                else{
                    if(userFainted == YES){
                        //NO SWITCHES,ENEMY FIRST, USER FAINT, ENEMY AWAKE, USER HIT, ENEMY HIT
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n \n %@ has fainted.",currentEnemyPokemon.name, enemyMove, currentUserPokemon.name];
                    }
                    else if(enemyFainted == YES){
                        //NO SWITCHES,ENEMY FIRST, ENEMY FAINT, ENEMY AWAKE, USER HIT, ENEMY HIT
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n \n %@ used %@. \n\n %@ has fainted.",currentEnemyPokemon.name, enemyMove, currentUserPokemon.name, userMove, currentEnemyPokemon.name];
                    }
                    else{
                        //NO SWITCHES,ENEMY FIRST, NO FAINT, ENEMY AWAKE, USER HIT, ENEMY HIT
                        currentAction = [NSString stringWithFormat:@"%@ used %@. \n \n %@ used %@.",currentEnemyPokemon.name, enemyMove, currentUserPokemon.name, userMove];
                    }
                }
                
            }
        }
    }

    [moveBox setTitle:currentAction forState:UIControlStateNormal];
    [self resetDialogueFields];

}

- (void) waitUntilDialogueClicked{

    [self showMoves];
    [moveBox setHidden: YES];
    [self enableSwitches];

}

- (void) hideMoves{
    [move1 setHidden:YES];
    [move2 setHidden:YES];
    [move3 setHidden:YES];
    [move4 setHidden:YES];
}

-(void) showMoves{
    [move1 setHidden:NO];
    [move2 setHidden:NO];
    [move3 setHidden:NO];
    [move4 setHidden:NO];
}

-(void) disableSwitches{
    [pokemon1 setEnabled:NO];
    [pokemon2 setEnabled:NO];
    [pokemon3 setEnabled:NO];
}

-(void) enableSwitches{
    [pokemon1 setEnabled:YES];
    [pokemon2 setEnabled:YES];
    [pokemon3 setEnabled:YES];
}

-(void) resetDialogueFields{
    userFirst = NO;
    enemyFirst = NO;
    enemyAsleep = NO;
    enemyWoke = NO;
    userMiss = NO;
    enemyMiss = NO;
    userFainted = NO;
    enemyFainted = NO;
    enemySwitched = NO;
    userSwitched = NO;
    userMove = @"";
    enemyMove = @"";
    userPokemonBeforeSwitch = @"";
    enemyPokemonBeforeSwitch = @"";
}
@end
