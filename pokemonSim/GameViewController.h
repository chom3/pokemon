//
//  GameViewController.h
//  pokemonSim
//

//  Copyright (c) 2016 Corey Hom. Stanley Chan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>


@interface GameViewController : UIViewController

@property (strong,nonatomic) AVAudioPlayer *audioPlayer;
@end
