//
//  Pokemon.h
//  pokemonSim
//
//  Created by Corey Hom on 4/4/16.
//  Copyright © 2016 Corey Hom. Stanley Chan. All rights reserved.
//
#import "Moves.h"

@interface Pokemon:NSObject
{
    NSString *name;
    
    int _curHP;
    int _maxHP;
    float _atk;
    float _def;
    float _special;
    int _speed;
    
    NSString *type1;
    NSString *type2;
    
    Move *move1;
    Move *move2;
    Move *move3;
    Move *move4;
    
    NSString *currentStatus;
    SKSpriteNode *image;
    
}
@property(nonatomic, readwrite) int _curHP;
@property(nonatomic, readwrite) int _maxHP;
@property(nonatomic, readwrite) float _atk;
@property(nonatomic, readwrite) float _def;
@property(nonatomic, readwrite) float _special;
@property(nonatomic, readwrite) int _speed;
@property(nonatomic, readwrite) SKSpriteNode *image;
@property(nonatomic, readwrite) NSString *name;
@property(nonatomic, readwrite) NSString *type1;
@property(nonatomic, readwrite) NSString *type2;
@property(nonatomic, readwrite) Move *move1;
@property(nonatomic, readwrite) Move *move2;
@property(nonatomic, readwrite) Move *move3;
@property(nonatomic, readwrite) Move *move4;
@property(nonatomic, readwrite) NSString *currentStatus;

@end

@implementation Pokemon

@synthesize _curHP, _maxHP,_atk, _def, _special, _speed;
@synthesize image;
@synthesize name, type1, type2, move1, move2, move3, move4;
@synthesize currentStatus;


-(id) init
{
    self = [super init];
    if (!self) return nil;
    return self;
}

- (id) initPokemon:(NSString*) nameIn
             curHP: (int) curHPIn
             maxHP: (int) maxHPIn
               atk: (float) atkIn
               def: (float) defIn
              spec: (float) specIn
               spd: (int) spdIn
             type1: (NSString*) type1In
             type2: (NSString*) type2In
             move1: (Move*) move1In
             move2:(Move*) move2In
             move3: (Move*) move3In
             move4: (Move*) move4In
     currentStatus: (NSString*) currentStatIn
             image: (SKSpriteNode*) imageIn
{
    self = [super init];
    if (self) {
        name = nameIn;
        _curHP = curHPIn;
        _maxHP = maxHPIn;
        _atk  =  atkIn;
        _def = defIn;
        _special = specIn;
        _speed = spdIn;
        type1 = type1In;
        type2 = type2In;
        move1 = move1In;
        move2 = move2In;
        move3 = move3In;
        move4 = move4In;
        currentStatus = currentStatIn;
        image = imageIn;
    }
    return self;
}

@end